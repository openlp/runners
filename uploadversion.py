#!/usr/bin/env python3
# -*- coding: utf-8 -*-

##########################################################################
# OpenLP - Open Source Lyrics Projection                                 #
# ---------------------------------------------------------------------- #
# Copyright (c) 2008-2020 OpenLP Developers                              #
# ---------------------------------------------------------------------- #
# This program is free software: you can redistribute it and/or modify   #
# it under the terms of the GNU General Public License as published by   #
# the Free Software Foundation, either version 3 of the License, or      #
# (at your option) any later version.                                    #
#                                                                        #
# This program is distributed in the hope that it will be useful,        #
# but WITHOUT ANY WARRANTY; without even the implied warranty of         #
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the          #
# GNU General Public License for more details.                           #
#                                                                        #
# You should have received a copy of the GNU General Public License      #
# along with this program.  If not, see <https://www.gnu.org/licenses/>. #
##########################################################################
"""
A script to get the OpenLP version from the source code and upload it to the appropriate file in
https://get.openlp.org/versions/
"""
import os
from contextlib import contextmanager
from io import StringIO
from subprocess import PIPE, CalledProcessError, run

from paramiko.client import SSHClient, AutoAddPolicy
from paramiko.rsakey import RSAKey


OPENLP_SERVER = 'openlp.io'
OPENLP_USER = 'openlp'
OPENLP_KEY = RSAKey.from_private_key(StringIO(os.environ['SSH_KEY']))
VERSIONS_DIR = '/home/{user}/sites/get.openlp.org/www/versions/'.format(user=OPENLP_USER)


def get_version():
    """Run setup.py and get the version number"""
    try:
        result = run(['python3', 'setup.py', '--version'], stdout=PIPE)
        return result.stdout.strip().decode('utf8')
    except CalledProcessError:
        return '0.0.0'


def get_version_file(full_version):
    """Return the correct version file for the version"""
    bits = full_version.split('.dev')
    current_version = {
        'full': full_version,
        'version': bits[0],
        'build': full_version.split('+')[1] if '+' in full_version else None
    }
    if current_version['build']:
        return 'nightly_version.txt'
    elif int(current_version['version'].split('.')[1]) % 2 != 0:
        return 'dev_version.txt'
    else:
        return 'version.txt'


@contextmanager
def sftp_connect():
    """Create an SSH connection to the server"""
    client = SSHClient()
    client.set_missing_host_key_policy(AutoAddPolicy())
    client.load_system_host_keys()
    client.connect(OPENLP_SERVER, username=OPENLP_USER, pkey=OPENLP_KEY)
    yield client.open_sftp()
    client.close()


def write_version_file(version, version_file):
    remote_filename = VERSIONS_DIR + version_file
    with sftp_connect() as sftp_client:
        remote_file = sftp_client.file(remote_filename, 'w')
        remote_file.write(version)


def main():
    """Run the script"""
    version = get_version()
    print('Version: {version}'.format(version=version))
    version_file = get_version_file(version)
    print('Version file: {version_file}'.format(version_file=version_file))
    write_version_file(version, version_file)


if __name__ == '__main__':
    main()
